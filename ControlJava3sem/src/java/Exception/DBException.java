package Exception;

/**
 * Created by Тимур on 23.11.2016.
 */
public class DBException extends Exception{
    public DBException() {
        super();
    }

    public DBException(String message) {
        super(message);
    }
}