package Services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import Exception.DBException;

public class DBWrapper {
    private final static String DRIVER = "org.postgresql.Driver";
    private final static String CONNECTION_URI = "jdbc:postgresql://localhost:5432/registerapp";
    private final static String LOGIN = "web";
    private final static String PASSWORD = "258605";

    private static Connection conn;


    public static Connection getConnection() throws DBException {
        if (conn == null) {
            try {
                Class.forName(DRIVER);
                conn = DriverManager.getConnection(CONNECTION_URI, LOGIN, PASSWORD);
            } catch (ClassNotFoundException ex) {
                throw new DBException("Can't find DB driver.");
            } catch (SQLException ex) {
                throw new DBException("Can't connect to DB (" + ex.getErrorCode() + ": " + ex.getMessage() + ").");
            }

        }
        return conn;
    }
}
