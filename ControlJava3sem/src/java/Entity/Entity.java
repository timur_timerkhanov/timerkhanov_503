package Entity;

/**
 * Created by Тимур on 23.11.2016.
 */
public class Entity {
    long id;
    String strField1;
    String strField2;
    int phoneNumber;
    String choice;
    Boolean bool;

    public Entity(int id, String strField1, String strField2, int phoneNumber, Boolean bool) {
        this.id = id;
        this.strField1 = strField1;
        this.strField2 = strField2;
        this.phoneNumber = phoneNumber;

        this.bool = bool;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStrField1() {
        return strField1;
    }

    public void setStrField1(String strField1) {
        this.strField1 = strField1;
    }

    public String getStrField2() {
        return strField2;
    }

    public void setStrField2(String strField2) {
        this.strField2 = strField2;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getChoice() {
        return choice;
    }

    public void setChoice(String choice) {
        this.choice = choice;
    }

    public Boolean getBool() {
        return bool;
    }

    public void setBool(Boolean bool) {
        this.bool = bool;
    }


}
