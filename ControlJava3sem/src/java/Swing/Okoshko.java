package Swing;

import Entity.Entity;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Тимур on 23.11.2016.
 */
public class Okoshko extends JFrame {
    public Okoshko() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException, IOException {

        super("OkoshoNaSwinge");
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        JTextArea textArea = new JTextArea();

        JButton exitButton = new JButton("Exit");
        JButton clearButton = new JButton("Clear");
        JButton addUser = new JButton("Add USER");
        JButton colorButton = new JButton("SHOW ALL USERS");
        JButton fileChooser = new JButton("Choose custom BG");
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 1, 30, 15);

        JPanel myPanel = new JPanel();
        JTextField emailField = new JTextField(10);
        JTextField password = new JPasswordField(10);
        JTextField name = new JTextField(10);
        JTextField phone = new JTextField(10);
        JComboBox c = new JComboBox();
        JCheckBox checkBox = new JCheckBox("Subscription");
        c.addItem("Russia");
        c.addItem("Usa");





        myPanel.add(new JLabel("Name"));
        myPanel.add(name);
        myPanel.add(new JLabel("Email"));
        myPanel.add(emailField);
        myPanel.add(Box.createHorizontalStrut(10));
        myPanel.add(new JLabel("Password"));
        myPanel.add(password);
        BoxLayout boxLayout = new BoxLayout(myPanel, BoxLayout.Y_AXIS);
        myPanel.add(new JLabel("Phone Number"));
        myPanel.add(phone);
        myPanel.add(new JLabel("Country"));
        myPanel.add(c);
        myPanel.add(checkBox);


        myPanel.setLayout(boxLayout);


        ObjectMapper mapper = new ObjectMapper();
int i=0;
        addUser.addActionListener(e -> {
            int result = JOptionPane.showConfirmDialog(null, myPanel,
                    "Please Enter X and Y Values", JOptionPane.OK_CANCEL_OPTION);
            if (result == JOptionPane.OK_OPTION) {
                Entity entity = new Entity(2,name.getText(), emailField.getText(), Integer.parseInt( phone.getText()), Boolean.FALSE);
                try {
                    String jsonInString = mapper.writeValueAsString(entity);
                    int PORT = 3000;
                    InetAddress address = InetAddress.getByName("localhost");
                    Socket socket = new Socket(address, PORT);
                    PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
                    printWriter.println("POST localhost:3000/users/2 HTTP/1.0");
                    printWriter.println(jsonInString);
                    printWriter.flush();

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        exitButton.addActionListener(e ->
                System.exit(0)
        );










        String[] columnNames = {"id", "First Name",
                "Email",
                "# of Years",
                "Vegetarian",
        "Button1"};
        Object[][] data = {
                {"Kathy", "Smith",
                        "Snowboarding", new Integer(5), new Boolean(false)},
                {"John", "Doe",
                        "Rowing", new Integer(3), new Boolean(true)},
                {"Sue", "Black",
                        "Knitting", new Integer(2), new Boolean(false)},
                {"Jane", "White",
                        "Speed reading", new Integer(20), new Boolean(true)},
                {"Joe", "Brown",
                        "Pool", new Integer(10), new Boolean(false)}
        };
        DefaultTableModel dm = new DefaultTableModel();
        dm.setDataVector(new Object[][]{{"Edit", "Delete", "foo"},
                {"Edit", "Delete", "bar"}}, new Object[]{"EditButton", "DeleteButton", "Integer", "String", "String", "Boolean" });
        JTable table = new JTable(dm);
//        table.setFillsViewportHeight(true);
        TableCellRenderer buttonRenderer = new ButtonRenderer();

        table.getColumn("EditButton").setCellRenderer(buttonRenderer);
        table.getColumn("EditButton").setCellEditor(new ButtonEditor(new JCheckBox()));
        table.getColumn("DeleteButton").setCellRenderer(buttonRenderer);
        table.getColumn("DeleteButton").setCellEditor(new ButtonEditor(new JCheckBox()));

        JScrollPane scrollPane = new JScrollPane(table);
        TableColumn column = null;
//        for (int i = 0; i < 6; i++) {
//            column = table.getColumnModel().getColumn(i);
//            if (i == 2) {
//                column.setPreferredWidth(100); //third column is bigger
//            } else {
//                column.setPreferredWidth(50);
//            }
//        }


        JPanel panel = new JPanel();
        add(panel);

//        scrollPane.add(table);
        panel.add(scrollPane);














        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()

                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(clearButton))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(exitButton))

                        ).addComponent(scrollPane))

                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(addUser)
                        .addComponent(colorButton)
                        .addComponent(fileChooser)
                        .addComponent(slider)
                ));
        layout.linkSize(SwingConstants.HORIZONTAL, addUser, colorButton, fileChooser, slider);
        layout.linkSize(SwingConstants.HORIZONTAL, clearButton, exitButton);
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(clearButton)
                        .addComponent(exitButton))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(scrollPane)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(addUser))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(colorButton))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(fileChooser))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(slider)
                                ))));

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();
        setVisible(true);

    }
}