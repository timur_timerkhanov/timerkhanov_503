package Server;


import java.io.*;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

class MyHandler extends Thread implements Handler {
    private static Vector<String> names = new Vector<>();
    private static Vector<PrintWriter> writers = new Vector<>();
    private Vector messageQueue = new Vector();
    private String name;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private FileWriter writer;
    BufferedReader reader;


    public MyHandler(Socket socket) {
        this.socket = socket;
        try {
            writer = new FileWriter("C:\\chat\\src\\history.txt", true);
            reader = new BufferedReader(new FileReader("C:\\chat\\src\\history.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void addClient() {
        try {
            boolean checked = false;
            out.println("NAMEPlease, choose your nickname ^^");
            while (!checked) {
                name = in.readLine();
                if (name == null || name.contains(" ") || name.isEmpty()) {
                    out.println("NAMEIncorrect name, please try again");
                    continue;
                }
                if (!names.contains(name)) {
                    names.add(name);
                    out.println("NAMEHello, " + name + "! Welcome to the cute chat");
                    checked = true;
                } else {
                    out.println("NAMEIncorrect name, please try again");
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized String getNextMessageFromQueue() throws InterruptedException {
        while (messageQueue.size() == 0)
            wait();
        String message = (String) messageQueue.get(0);
        messageQueue.removeElementAt(0);
        return message;
    }

    private String getDate() {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
        return sdf.format(date);
    }

    public synchronized void sendMessageToClients(String message) {
        for (PrintWriter writer1 : writers) {
            writer1.println(getDate() + " " + name + ": " + message + "\n");
        }
    }

    private synchronized void printHistory() throws IOException {
        String line = "";
        while (line != null) {
                out.println(line);
                line = reader.readLine();
            }
    }

    @Override
    public void run() {
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);

            addClient();
            out.println("ACCEPTED");
            writers.add(out);  // Добавляем пользователя в список активных соединений
            printHistory();

            // В цикле получаем сообщение и ретранслируем его для всех пользователей
            while (true) {
                String input = in.readLine();
                if (input.equals("EXITPLEASEEXIT")) {
                    out.print(name + " has left this room");
                    break;
                }
                writer.write(getDate() + " " + name + ": " + input + "\n");
                writer.flush();
                messageQueue.add(input);
                String message = getNextMessageFromQueue();
                sendMessageToClients(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (name != null)
                names.remove(name);
            if (out != null)
                writers.remove(out);
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
