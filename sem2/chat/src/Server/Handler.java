package Server;

interface Handler {
    void addClient();
    String getNextMessageFromQueue() throws InterruptedException;
    void sendMessageToClients(String message);
}
