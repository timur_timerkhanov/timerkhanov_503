package Server;

import Server.MyHandler;

import java.io.IOException;
import java.net.ServerSocket;

public class server {
    private static final int PORT = 9001;

    public static void main(String[] args) throws IOException {
        System.out.println("Chat Server.server is running");
        try (ServerSocket listener = new ServerSocket(PORT)) {
            while (true) {
                new MyHandler(listener.accept()).start();
            }
        }
    }
}
