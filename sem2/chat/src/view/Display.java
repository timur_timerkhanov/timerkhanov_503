package view;

interface Display {
    void display(String message);
}
