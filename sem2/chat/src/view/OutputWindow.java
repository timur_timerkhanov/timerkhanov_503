package view;

import java.io.PrintWriter;
import java.util.Scanner;

public class OutputWindow {
    PrintWriter out;
    public OutputWindow(PrintWriter out) {
        Scanner sc = new Scanner(System.in);
        this.out = out;
        String msg = "";
        while (!msg.equals("exit")) {
            msg = sc.nextLine();
            out.println(msg);
        }
        out.print("EXITPLEASEEXIT");
    }
}
