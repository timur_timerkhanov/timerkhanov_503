package view;

import model.Client;

public class myObserver implements Observer {
    String message;
    Client client;
    private Display display;

    public myObserver(Client client, Display d) {
        this.client = client;
        client.registerObserver(this);
        display = d;
    }

    @Override
    public void update(String message) {
        this.message = message;
        display.display(message);
    }
}
