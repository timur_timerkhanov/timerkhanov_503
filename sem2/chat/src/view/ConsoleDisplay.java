package view;
public class ConsoleDisplay implements Display {
    @Override
    public void display(String message) {
        if (message.startsWith("NAME")) {
            System.out.println(message.substring(4));
            return;
        }
        if (message.startsWith("ACCEPTED"))
            return;
        System.out.println(message);
    }
}

