import model.Client;
import view.Observer;
import view.*;

import java.io.IOException;
import java.io.PrintWriter;

class Main {

    public static void main(String[] args) throws IOException {

        Client client = new Client("192.168.1.2"); // Подклбчились к серверу

        ConsoleDisplay d = new ConsoleDisplay();
        Observer consoleObserver = new myObserver(client,d); // Создали обсервер
        client.connect();
        PrintWriter out = client.getOutputStream();
        OutputWindow outputWindow = new OutputWindow(out);
    }
}




