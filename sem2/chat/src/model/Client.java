package model;

import view.Observer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class Client implements Subject {
    private ArrayList<Observer> observers;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private String message;

    public Client(String serverAddress) throws IOException {
        observers = new ArrayList<>();
        socket = new Socket(serverAddress, 9001);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
    }

    public void connect() throws IOException {
        Receiver receiver = new Receiver();
        receiver.start();
    }

    public void messageReceived() {
        notifyObservers();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer1 : observers) {
            observer1.update(message);
        }
    }

    public PrintWriter getOutputStream() {
        return this.out;
    }

    class Receiver extends Thread {
        @Override
        public void run() {
            try {
                while (true) {
                    message = in.readLine();
                    messageReceived();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
