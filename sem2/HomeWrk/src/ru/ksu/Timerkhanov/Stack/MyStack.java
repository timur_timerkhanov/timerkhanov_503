package ru.ksu.Timerkhanov.Stack;

public class MyStack<T> implements Stack<T> {
    private T[] elementArray= (T[]) new Object[10];
    private int currentCount=0;
    public void push(T element) {
        elementArray[currentCount]=element;
        currentCount++;
        if (currentCount>elementArray.length-1){
            T[] newArr = (T[]) new Object[2*elementArray.length];
            System.arraycopy(this.elementArray,0, newArr, 0, currentCount);
            this.elementArray=newArr;
        }
    }
    public T pop(){
        currentCount--;
        return elementArray[currentCount-1];
    }

    public T peek() {
        return elementArray[currentCount-1];
    }

    public boolean isEmpty(){
        return currentCount==0;
    }
}
