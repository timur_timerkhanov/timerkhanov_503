package ru.ksu.Timerkhanov.Queue;
public class QueueEmptyException extends RuntimeException {
    public QueueEmptyException (Throwable e) {
        initCause(e);
    }
}
