package ru.ksu.Timerkhanov.Queue;
public class PriorityQueue<T> implements Queue<T> {
    private T[] elementArray = (T[]) new Object[5];
    private int[] priorityArray = new int[elementArray.length];
    private int currentCount = 0;
    private int index = 0;
    Exception e = new Exception();


    public void add(T element, int p) {

        currentCount++;

        int ind = 0;
        for (int i = 0; i < elementArray.length; i++) {
            if (priorityArray[i] <= p) {
                continue;
            } else ind = i;

        }
        if (currentCount + ind > this.priorityArray.length - 1) {
            T[] newArr = (T[]) new Object[2 * (elementArray.length - index)];
            int[] newArrP = new int[2 * (elementArray.length - index)];
            System.arraycopy(this.elementArray, index, newArr, 0, elementArray.length - index);
            System.arraycopy(this.priorityArray, index, newArrP, 0, priorityArray.length - index);
            this.elementArray = newArr;
            this.priorityArray = newArrP;
            currentCount -= index;
            index = 0;
        }

        System.arraycopy(this.priorityArray, ind, this.priorityArray, ind + 1, currentCount - ind - 1);
        priorityArray[ind] = p;

        System.arraycopy(this.elementArray, ind, this.elementArray, ind + 1, currentCount - ind - 1);
        elementArray[ind] = element;


    }

    public T poll() {
        if (index == currentCount) {
            throw new QueueEmptyException(e);
        }
        index++;
        return elementArray[index - 1];
    }

    public T peek() {
        return elementArray[index];
    }

    public int size() {
        return currentCount - index;
    }
}

