package ru.ksu.Timerkhanov.Queue;
public class MyQueue<T> implements Queue<T> {
    private T[] elementArray= (T[]) new Object[10];
    private int currentCount=0;
    private int index=0;
    Exception e = new Exception();
    public void add (T element, int p) {
        elementArray[currentCount]=element;
        currentCount++;
        if (currentCount> elementArray.length-1) {
            T[] newArr= (T[]) new Object[2*(elementArray.length - index)];
            System.arraycopy(this.elementArray, index, newArr, 0, elementArray.length-index);
            this.elementArray=newArr;
            currentCount-=index;
            index=0;
        }
    }

    public T poll() {
        if (index==currentCount) {
            throw new QueueEmptyException(e);
        }
        index++;
        return elementArray[index-1];
    }

    public T peek() {
        return elementArray[index];
    }

    public int size() {
        return currentCount-index;
    }
}
