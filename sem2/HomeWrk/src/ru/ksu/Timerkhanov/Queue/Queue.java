package ru.ksu.Timerkhanov.Queue;
public interface Queue<T> {
    void add (T element, int p);

    T poll ();
    T peek ();
    int size();
}