package ru.ksu.Timerkhanov.binarySearch;

import java.util.Arrays;
import java.util.Random;

public class main {
    public static void main(String[] args) {
        int[] arr = new int[10];
        Random random = new Random();

        for (int i = 0; i < arr.length; i++) {
            int rand = random.nextInt(10);
            arr[i] = rand;
        }
        Arrays.sort(arr);
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
        System.out.println();

        search search = new search();
        System.out.println(search.find(5, 0, arr.length, arr));

    }
}

class search {
    public int find(int el, int lo, int hi, int[] arr) {
        if (el > arr[arr.length - 1]) {
            return -1;
        }
        if (hi < lo) {
            return -1;
        }
        int mid = lo + (hi - lo) / 2;
        if (el < arr[mid]) {
            return find(el, lo, mid - 1, arr);
        } else if (el > arr[mid]) {
            return find(el, mid + 1, hi, arr);
        } else return mid;

    }
}
