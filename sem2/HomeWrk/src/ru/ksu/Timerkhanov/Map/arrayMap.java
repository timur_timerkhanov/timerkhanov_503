package ru.ksu.Timerkhanov.Map;
public class arrayMap<K, V> implements Map<K, V> {
    static int size = 0;
    private K[] keys;
    private V[] values;

    arrayMap(int n) {           //constructor which creates an array with n-size
        keys = (K[]) new Object[n];
        values = (V[]) new Object[n];
    }

    @Override
    public void add(K key, V value) {
        keys[size] = key;
        values[size] = value;
        size++;
    }

    @Override
    public V get(K key) {
        int index;
        for (int i = 0; i < size; i++) {
            if (keys[i] != null) {
                if (keys[i].equals(key)) {
                    index = i;
                    return values[index];
                }
            }
        }
        return null;
    }

    @Override
    public void remove(K key) {
        for (int i = 0; i < keys.length; i++) {
            if (keys[i] != null) {
                if (keys[i].equals(key)) {
                    System.arraycopy(keys, i + 1, keys, i, size - i);          //shifting array in order to remove element
                    System.arraycopy(values, i + 1, values, i, size - i);
                    size--;
                }
            }
        }
    }
}
