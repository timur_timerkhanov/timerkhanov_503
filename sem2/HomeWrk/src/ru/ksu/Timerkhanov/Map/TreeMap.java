
package ru.ksu.Timerkhanov.Map;
class TreeMap<K extends Comparable, V> implements Map<K, V> {
    private K key;
    private V value;
    private TreeMap<K, V> rightSibling;
    private TreeMap<K, V> leftSibling;

    public TreeMap(K t, V value) {
        this.key = t;
        this.value = value;
    }

    private TreeMap<K, V> root = null;

    public void leftTurn(){
        TreeMap<K,V> oldRoot = root;
        TreeMap<K,V> node = root.rightSibling.leftSibling;
        oldRoot.rightSibling = node.leftSibling;
        root = node;
        root.leftSibling = oldRoot;
    }

    public void rightTurn(){
        TreeMap<K,V> oldRoot = root;
        TreeMap<K,V> node = root.leftSibling.rightSibling;
        oldRoot.rightSibling = node.leftSibling;
        root = node;
        root.rightSibling = oldRoot;

    }

    public void add(K k, V v) {
        TreeMap<K, V> x = root, y = null;
        while (x != null) {
            int cmp = k.compareTo(x.key);
            if (cmp == 0) {
                x.value = v;
                return;
            } else {
                y = x;
                if (cmp < 0) {
                    x = x.leftSibling;
                } else x = x.rightSibling;
            }
        }
        TreeMap<K, V> newNode = new TreeMap<K, V>(k, v);
        if (y == null) {
            root = newNode;
        } else {
            if (k.compareTo(y.key) < 0) {
                y.leftSibling = newNode;
            } else y.rightSibling = newNode;
        }
    }


    public void remove(K k) {
        TreeMap<K, V> x = root;
        TreeMap<K, V> y = null;
        while (x != null) {
            int cmp = k.compareTo(x.key);
            if (cmp == 0) {
                break;
            } else {
                y = x;
                if (cmp < 0) {
                    x = x.leftSibling;
                } else x = x.rightSibling;
            }
        }
        if (x == null) {
            return;
        }
        if (x.rightSibling == null) {
            if (y == null) {
                root = x.leftSibling;
            } else if (x == y.leftSibling) {
                y.leftSibling = x.leftSibling;
            } else y.rightSibling = x.leftSibling;
        } else {
            TreeMap<K, V> leftMin = x.rightSibling;
            y = null;
            while (leftMin.leftSibling != null) {
                y = leftMin;
                leftMin = leftMin.leftSibling;
            }
            if (y != null) {
                y.leftSibling = leftMin.rightSibling;
            } else {
                x.rightSibling = leftMin.rightSibling;
            }
            x.key = leftMin.key;
            x.value = leftMin.value;
        }
    }

    public V get(K key) {
        TreeMap<K, V> x = root;
        while (x != null) {
            int cmp = key.compareTo(x.key);
            if (cmp == 0) return x.value;
            if (cmp < 0) {
                x = x.leftSibling;
            } else x = x.rightSibling;
        }
        return null;
    }





}