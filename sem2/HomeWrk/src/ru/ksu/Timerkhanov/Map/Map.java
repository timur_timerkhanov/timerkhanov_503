package ru.ksu.Timerkhanov.Map;
public interface Map<K,V> {
    void add (K key, V value);
    V get (K key);
    void remove (K key);
}
