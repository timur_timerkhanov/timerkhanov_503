package ru.ksu.Timerkhanov.Exception;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class main {
    public static void main(String[] args) {
       /* Object[] arr = new Object[5];  //NullPointer (unchecked, runtime)
        arr[4]=null;
        int c = (int)arr[4] + 5;*/

        int a = 0;                     //Arithmetic (unchecked,runtime)
        int b = 24;
        try {
            System.out.println(b / a);

        } catch (ArithmeticException e) {
            System.out.println("Ne mogu");
//            throw e;
        }

         finally {
            System.out.println("A eto mogu");
        }


//        FileInputStream f = new FileInputStream("C:/textfasfas.txt");


        int[] m = new int[10];                      //try-catch-catch
        Scanner sc = new Scanner(System.in);
        try {
            int g = sc.nextInt();
            m[g] = 4/g;
            System.out.println(m[g]);
        } catch (ArithmeticException e1) {
            System.out.println("Произошла недопустимая арифметическая операция");
        } catch (ArrayIndexOutOfBoundsException e1) {
            System.out.println("Обращение по недопустимому индексу массива");
        }

        System.out.println("Выполняюсь, если пойманы все checked и нет unchecked ");

    }
}
