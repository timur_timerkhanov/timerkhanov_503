package ru.ksu.Timerkhanov.List;
import ru.ksu.Timerkhanov.List.genericList.List;

/**
 * Created by Тимур on 10.02.2016.
 */
public class ArrayList<T> implements List<T> {
    private T[] elementArray= (T[]) new Object[10];
    private int currentIndex = 0;
    private void scaleArray(){
        int length = this.elementArray.length;
        int newLength = 3*length/2 + 1;
        T[] newArr = (T[]) new Object[newLength];
       /* for(int i = 0; i<length; i++) {
            newArr[i]=this.elementArray[i];
        }*/
        System.arraycopy(this.elementArray, 0, newArr, 0, length);
        this.elementArray=newArr;
    }
    public void add ( T el){
        elementArray[currentIndex]=el;
        this.elementArray[currentIndex] = el;
        currentIndex++;

        if(currentIndex==this.elementArray.length){
            this.scaleArray();
        }


    }
    public boolean isEmpty(){
        return currentIndex==0;
    }
    public T getAt(int index){
        if(index>=currentIndex){
            throw new ArrayIndexOutOfBoundsException();
        }
        return this.elementArray[index];

    }
    public void remove(int index) {
        int size=this.elementArray.length;
        int numMoved = size - index - 1;
      /*  for (int i=index; i<currentIndex; i++) {
            this.elementArray[i]=this.elementArray[i+1];
        }*/
        currentIndex--;

        if (numMoved>0){
            System.arraycopy(this.elementArray, index+1, this.elementArray, index, numMoved);
//            this.elementArray = newArr;
        }

        if (((size-currentIndex)/size)>0.3) {
            T[] newArr = (T[]) new Object[currentIndex];
            System.arraycopy(this.elementArray, 0, newArr, 0, this.elementArray.length );
            this.elementArray=newArr;

        }
    }
    public int getSize(){
        return this.currentIndex;
    }
    public void set(int index, T el){
        if(index>=currentIndex){
            throw new ArrayIndexOutOfBoundsException();
        }
        this.elementArray[index]=el;
    }
    public void clear () {
        for (int i=0; i<currentIndex; i++) {
            this.elementArray[i]=null;
        }
        currentIndex=0;
        T[] newArr = (T[]) new Object[10];
        this.elementArray=newArr;
    }
}
