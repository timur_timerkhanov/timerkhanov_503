package ru.ksu.Timerkhanov.List;
import ru.ksu.Timerkhanov.List.genericList.List;

public class LinkedList<T> implements List<T> {

    private Link head;
    private Link lastLink;
    private int size = 0;

    public void add(T el) {
        size++;
        if (head == null) {
            head = new Link(el);
            lastLink = head;
            return;
        }
        Link newLink = new Link(el);
        lastLink.setNext(newLink);
        lastLink = newLink;
    }

    public boolean isEmpty() {
        return (head == null);
    }

    public T getAt(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        Link<T> l = head;
        for (int i = 0; i < index; i++) {
            l = l.getNext();
        }
        return l.getValue();
    }

    public void remove(int index) {
        if (index > size || index < 0)
            throw new ArrayIndexOutOfBoundsException();
        Link l = head;
        for (int i = 0; i < index - 1; i++) {
            l = l.getNext();
        }
        Link tolink = l.getNext().getNext();
        l.setNext(tolink);
        size--;
    }

    public int getSize() {
        return size;
    }

    public void set(int index, T el) {
        Link l = head;
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        for (int i = 0; i < index; i++) {
            l = l.getNext();
        }
        l.setValue(el);
    }

    public void clear() {
        lastLink = head;
        size = 0;
    }

}

class Link<T> {
    private T value;

    public Link(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T el) {
        this.value = el;
    }

    private Link next;

    public void setNext(Link l) {
        next = l;
    }

    public Link getNext() {
        return next;
    }

}
