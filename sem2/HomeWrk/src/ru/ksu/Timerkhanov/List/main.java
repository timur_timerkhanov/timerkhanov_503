package ru.ksu.Timerkhanov.List;
import ru.ksu.Timerkhanov.List.genericList.List;
import java.util.Random;


public class main {
    public static void main(String[] args) {
        long startTime = System.nanoTime();
        List<Person> l = new ArrayList<>();
        checkList(l);
        long stopTime = System.nanoTime();
        System.out.println("Time: " + (stopTime - startTime));

    }

    public static void checkList(List<Person> l) {
        Random r = new Random();
        int random = r.nextInt();
        for (int i = 0; i < 1000000; i++) {
            l.add(new Person(10 + i));
        }


        System.out.println("Old size: " + l.getSize());
        System.out.println(l.isEmpty());


      /*  for (int i = 0; i < 10; i++) {
            System.out.println(l.getAt(i).age);
        }*/

        System.out.println();

        for (int i = 0; i < 6; i++) {
            l.remove(2);
        }

        System.out.println("New size is: " + l.getSize());
        for (int i = 0; i < 4; i++) {
            System.out.println(l.getAt(i).age);
        }


    }
}
    class Person {
        public int age;
        public Person(int age){
            this.age = age;
        }
    }


