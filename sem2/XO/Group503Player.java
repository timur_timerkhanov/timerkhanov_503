import java.util.Random;

/*
class Group503Player extends Player {
    private Random random = new Random();
    private int nameNumber;

    public String getName() {
        return "BlackBot";
    }

    public void move(MoveContext context) {

        if (this.getIsX()) {
            if (checkRows(context, 'x', 0)) {
                return;
            }
            //cols check
            if (checkCols(context, 'x', 0)) {
                return;
            }
            //main diagonal
            if (context.getField(0, 0) == 'x') {
                if (context.getField(1, 1) == 'x' && context.setX(2, 2)) {
                    return;
                }
                if (context.getField(2, 2) == 'x' && context.setX(1, 1)) {
                    return;
                }

            } else if (context.getField(1, 1) == 'x') {
                if (context.getField(2, 2) == 'x' && context.setX(0, 0)) {
                    return;
                }
            }
            //secondary diagonal
            if (context.getField(0, 2) == 'x') {
                if (context.getField(1, 1) == 'x' && context.setX(2, 0)) {
                    return;
                }
                if (context.getField(2, 0) == 'x' && context.setX(1, 1)) {
                    return;
                }

            } else if (context.getField(1, 1) == 'x') {
                if (context.getField(2, 2) == 'x' && context.setX(0, 0)) {
                    return;
                }
            }
            if (checkRows(context, 'o', 0)) {
                return;
            }
            //cols check
            if (checkCols(context, 'o', 0)) {
                return;
            }
            //main diagonal
            if (context.getField(0, 0) == 'o') {
                if (context.getField(1, 1) == 'o' && context.setX(2, 2)) {
                    return;
                }
                if (context.getField(2, 2) == 'o' && context.setX(1, 1)) {
                    return;
                }
            } else if (context.getField(1, 1) == 'o') {
                if (context.getField(2, 2) == 'o' && context.setX(0, 0)) {
                    return;
                }
            }
            //secondary diagonal
            if (context.getField(0, 2) == 'o') {
                if (context.getField(1, 1) == 'o' && context.setX(2, 0)) {
                    return;
                }
                if (context.getField(2, 0) == 'o' && context.setX(1, 1)) {
                    return;
                }

            } else if (context.getField(1, 1) == 'o') {
                if (context.getField(2, 2) == 'o' && context.setX(0, 0)) {
                    return;
                }
            }

            if (context.getField(1, 1) == 0) {
                context.setX(1, 1);
                return;
            } else if (context.getField(1, 1) == 'x') {
                if (context.setX(0, 0) || context.setX(2, 2) || context.setX(0, 2) || context.setX(2, 0)) {
                    return;
                } else if (context.setX(0, 1) || context.setX(1, 0) || context.setX(1, 2) || context.setX(2, 1)) {
                    return;
                }
            } else {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        if (context.setX(i, j)) {
                            return;
                        }
                    }
                }
            }
        } else {
            if (checkRowsO(context, 'o', 0)) {
                return;
            }
            //cols check
            if (checkColsO(context, 'o', 0)) {
                return;
            }
            //main diagonal
            if (context.getField(0, 0) == 'o') {
                if (context.getField(1, 1) == 'o' && context.setO(2, 2)) {
                    return;
                }
                if (context.getField(2, 2) == 'o' && context.setO(1, 1)) {
                    return;
                }

            } else if (context.getField(1, 1) == 'o') {
                if (context.getField(2, 2) == 'o' && context.setO(0, 0)) {
                    return;
                }
            }
            //secondary diagonal
            if (context.getField(0, 2) == 'o') {
                if (context.getField(1, 1) == 'o' && context.setO(2, 0)) {
                    return;
                }
                if (context.getField(2, 0) == 'o' && context.setO(1, 1)) {
                    return;
                }

            } else if (context.getField(1, 1) == 'o') {
                if (context.getField(2, 2) == 'o' && context.setO(0, 0)) {
                    return;
                }
            }

            if (checkRowsO(context, 'x', 0)) {
                return;
            }
            //cols check
            if (checkColsO(context, 'x', 0)) {
                return;
            }
            //main diagonal
            if (context.getField(0, 0) == 'x') {
                if (context.getField(1, 1) == 'x' && context.setO(2, 2)) {
                    return;
                }
                if (context.getField(2, 2) == 'x' && context.setO(1, 1)) {
                    return;
                }

            } else if (context.getField(1, 1) == 'x') {
                if (context.getField(2, 2) == 'x' && context.setO(0, 0)) {
                    return;
                }
            }
            //secondary diagonal
            if (context.getField(0, 2) == 'x') {
                if (context.getField(1, 1) == 'x' && context.setO(2, 0)) {
                    return;
                }
                if (context.getField(2, 0) == 'x' && context.setO(1, 1)) {
                    return;
                }
            } else if (context.getField(1, 1) == 'x') {
                if (context.getField(2, 2) == 'x' && context.setO(0, 0)) {
                    return;
                }
            }

            if (context.getField(1, 1) == 0) {
                context.setO(1, 1);
                return;
            } else if (context.getField(1, 1) == 'o') {
                if (context.setO(0, 0) || context.setO(2, 2) || context.setO(0, 2) || context.setO(2, 0)) {
                    return;
                } else if (context.setO(0, 1) || context.setO(1, 0) || context.setO(1, 2) || context.setO(2, 1)) {
                    return;
                }

            } else {
                if (context.setO(0, 0) || context.setO(2, 2) || context.setO(0, 2) || context.setO(2, 0)) {
                    return;
                }
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        if (context.setO(i, j)) {
                            return;
                        }
                    }
                }
            }
        }
    }

    public boolean checkRows(MoveContext context, char p, int j) {
        if (j == 3) return false;
        if (context.getField(0, j) == p) {
            if (context.getField(1, j) == p && context.setX(2, j)) {
                return true;
            } else if (context.getField(2, j) == p && context.setX(1, j)) {
                return true;
            }
        } else if (context.getField(1, j) == p) {
            if (context.getField(2, j) == p && context.setX(0, j)) {
                return true;
            }
        }
        checkRowsO(context, p, j + 1);
        return false;
    }

    public boolean checkCols(MoveContext context, char p, int i) {
        if (i == 3) return false;
        if (context.getField(i, 0) == p) {
            if (context.getField(i, 1) == p && context.setX(i, 2)) {
                return true;
            }
            if (context.getField(i, 2) == p && context.setX(i, 1)) {
                return true;
            }
        } else if (context.getField(i, 1) == p) {
            if (context.getField(i, 2) == p && context.setX(i, 0)) {
                return true;
            }
        }
        checkColsO(context, p, i + 1);
        return false;
    }

    public boolean checkColsO(MoveContext context, char p, int i) {
        if (i == 3) return false;
        if (context.getField(i, 0) == p) {
            if (context.getField(i, 1) == p && context.setO(i, 2)) {
                return true;
            }
            if (context.getField(i, 2) == p && context.setO(i, 1)) {
                return true;
            }
        } else if (context.getField(i, 1) == p) {
            if (context.getField(i, 2) == p && context.setO(i, 0)) {
                return true;
            }
        }
        checkColsO(context, p, i + 1);
        return false;
    }

    public boolean checkRowsO(MoveContext context, char p, int j) {
        if (j == 3) return false;
        if (context.getField(0, j) == p) {
            if (context.getField(1, j) == p && context.setO(2, j)) {
                return true;
            } else if (context.getField(2, j) == p && context.setO(1, j)) {
                return true;
            }
        } else if (context.getField(1, j) == p) {
            if (context.getField(2, j) == p && context.setO(0, j)) {
                return true;
            }
        }
        checkRowsO(context, p, j + 1);
        return false;
    }
}


*/
