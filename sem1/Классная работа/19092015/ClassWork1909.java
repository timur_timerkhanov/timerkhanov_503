import java.util.*;
public class ClassWork1909 {
    public static void main(String[] args) {
        Random random = new Random();
        int age = random.nextInt(100); // checking conditions
        if(age >= 18) {
            System.out.println("Too old");
        }else {System.out.println("Young enough");
        }   int a = random.nextInt(30);

        int x = a > 15? a*a+a : a*a-a; // trying ternary operator
        System.out.println(x);
        System.out.println("a= "+a);

        int depth = 0;         // trying cycles, digging a pit
        while (depth < 15) {
            depth++;
            System.out.println("1 m was digged " +(15-depth)+" left");
        }

        do {                  //using cycle with postcondition
            depth++;
            System.out.println("1 m was digged " +(15-depth)+" left");
        }  while (depth < 15);

        int depth15=15;      // trying for-conditions, break, continue operators
        for ( int i = 0; i<=15; i++) { System.out.println("1 m was digged " + (depth15-i)+ " left");
            if (i==10) break;    // TODO: use "continue" instead of "break"
        }
        int z = 2515679;
        int j = 0;
        int p = z;

        while (p > 0) {
            p = (p / 10);
            j = j + 1;
        }

        int count = 0;
        int k =0;
        int digit=0;

        while (j > 0) {
            int l = (int)Math.pow(10,j-1);
            k=z/l;
            digit=k%10;
            j--;

            System.out.println("digit= "+ digit);

        }


        int o = 2;                 // realizing involution function
        int m =6;
        int b=0;
        int u=1;
        while (b!=m) {u=u*o; b++;}
        System.out.println(u);

        int q =9;
        int n = 89000;
        while (n>0) { n=n/q;
            q=q/3;
            System.out.println(n);
            if (q==0) {System.out.println("q = 0");
                q=q+10; continue;}
        }
    }
}
