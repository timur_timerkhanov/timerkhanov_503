/**
 * Created by ����� on 24.10.2015.
 */
public class Recursion {
    public static void main (String[] args) {
        int index=0;
        int[] x = {0,1,5,7,9,90,20,40};
        arrSqr(x,index);
        for (int el:x){
            System.out.println(el);
        }

    }
    public static void arrSqr(int[] array, int index) {
        array[index]*=array[index];
        if(array.length-1==index) {
            return;
        }
        arrSqr(array, ++index);
    }
}
