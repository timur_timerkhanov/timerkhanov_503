public abstract class Animal {
    public static void main(String[] args) {
        Animal Vasya = new Cat();
        Vasya.est();
        Animal Sam = new Dog();
        Sam.sound();
    }

    public abstract void est();
    public abstract void sound();
}
    class Cat extends Animal {
        public void est() {
            System.out.println("Whiskas--;");
        }


        public void sound() {
            System.out.println("Mew mew");

        }
    }
    class Dog extends Animal {
            public void est() {
                System.out.println("Pedigree--;");
            }


            public void sound() {
                System.out.println("Haw Haw");

            }
        }



