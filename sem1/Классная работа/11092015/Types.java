import java.util.*;
public class Types {
    public static void main(String[] args) {

        Random random = new Random(); // initializing random variables
        int a = random.nextInt(10);
        int b = random.nextInt(100);
        int c = random.nextInt(50);
        int d = random.nextInt(70);
        int r = random.nextInt(15001);
		
        System.out.println(r); // setting value 
        r /= 15;
        System.out.println(r);
        r -= 400;
        System.out.println(r);
        r += 1000;
        System.out.println(r);
        r %= 10;
        System.out.println(r);
		
        boolean t = true; // setting boolean value
        t&=false; System.out.println(t);
        t |=false; System.out.println(t);
        t &= true; System.out.println(t);
        t |=true; System.out.println(t);
		
		long m = Long.MAX_VALUE-10000000;
		int n = (int) m; System.out.println("setting max long to int " +n);
		
        boolean x = (a % 5==0) && (b>72); // elementary actions
        boolean y = ( a % 2 == 0) && (b% 2 == 0) && (c% 2 == 0) && (d% 2 == 0);
        boolean z = (a % 2 == 1) || (b% 2 == 1) || (c% 2 == 1) || ( d % 2 == 1);
        boolean u = (a+b+c>100);
        boolean p = (b*c*d>1000);
        boolean l = ((a+b+c) % 10 ==0);
		
        System.out.println("a= " +a +", 1= " + x);
        System.out.println("b= "+b+", 2= " + y);
        System.out.println("c= "+c+", 3= "+z);
        System.out.println("d= "+d+", 4= "+u);
        System.out.println("5= "+p);
        System.out.println("6= "+l);
    }
}