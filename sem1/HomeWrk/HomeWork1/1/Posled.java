import java.util.Scanner;


public class Posled {
    public static void main(String[] args) {
    Scanner nomer = new Scanner(System.in);
    int k = nomer.nextInt();
    int n = 0;
    int i = 1;
    int m;
	//checking all numbers while i != our count
    while (i <= k) {
        n++;
        m = n;              
		//checking if there are only 1 and 4 digits
        while (m > 0) {
            if ((m % 10 != 1) && (m % 10 != 4)) {  
                break;
            }

            m = m / 10;
        }
		//if number consisting of only 1 and 4, increasing i
        if (m == 0)
            i = i + 1;      

    }
    System.out.println(n);
}
}