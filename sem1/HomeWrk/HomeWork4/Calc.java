import java.util.Scanner;
public class Calc {
    public static void main(String args[]) {

        System.out.println("List of operations: +, -, *, /, sqr, sqrt, sin, cos, log, ^, mod(a), c(to clear the history), h(to see history)");
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, input a number");
        double inp1 = sc.nextDouble();   //inputting the first number
        String history = Double.toString(inp1);  //adding first number to history
        double result, inp2;
        result = inp1; //first result = first number
        String op = sc.nextLine();
        //inputting an operation
        //according to it call different methods
        //all of them returns values, which becoming new results
        //updating the history line by adding brackets, operation and two numbers
        while (true) {
            switch (op) {
                case "*":
                    System.out.println("Input number:");
                    inp2 = sc.nextDouble();
                    result = multi(result, inp2);
                    history="(" + history + ")" + op + inp2;
                    break;
                case "/":
                    System.out.println("Input number:");
                    inp2 = sc.nextDouble();
                    result = divide(result, inp2);
                    history = "(" + history +")" + op + inp2;
                    break;
                case "+":
                    System.out.println("Input number:");
                    inp2 = sc.nextDouble();
                    result = plus(result, inp2);
                    history = "(" + history + ")" + op + inp2;
                    break;
                case "-":
                    System.out.println("Input number:");
                    inp2 = sc.nextDouble();
                    result = substr(result, inp2);
                    history = "(" + history + ")" + op + inp2;
                    break;
                case "%":
                    System.out.println("Input number:");
                    inp2 = sc.nextDouble();
                    result = percent(result, inp2);
                    history = "(" + history + ")" + op + inp2;
                    break;
                case "sqr":
                    result = square(result);
                    history = "(" + history + ")" + op;
                    break;
                case "^":
                    System.out.println("Input number:");
                    inp2 = sc.nextDouble();
                    result = pow(result, inp2);
                    history = "(" + history + ")" +op + inp2;
                    break;
                case "log":
                    result = log(result);
                    history = op + "(" + history + ")";
                    break;
                case "sqrt":
                    result = sqrt(result);
                    history = op + "("+history+")";
                    break;
                case "mod":
                    System.out.println("Input number:");
                    inp2 = sc.nextDouble();
                    result = mod(result, inp2);
                    history = "(" + history + ")" + op + inp2;
                    break;
                case "cos":
                    result=cos(result);
                    history=op + "(" + history + ")";
                    break;
                case  "sin":
                    result=sin(result);
                    history=op + "(" + history + ")";
                    break;
                case "c":
                    System.out.println("History cleared.");
                    System.out.println("Input number:");
                    inp2 = sc.nextDouble();
                    result = inp2;
                    history=Double.toString(result);
                    break;
                case "h":
                    System.out.println("History of operations: " + history);
                    break;
            }
            System.out.println("Result: " + result);
            System.out.println("Choose operation: ");
            op=sc.next();
        }
    }

    private static double sin(double x) {
        double m = Math.sin(x);
        return m;
    }
    private static double cos(double x) {
        double m = Math.cos(x);
        return m;
    }
    private static double mod(double x, double y) {
        double m = x % y;
        return m;
    }
    private static double sqrt(double x) {
        double m=Math.sqrt(x);
        return m;
    }
    private static double log(double x) {
        double m=Math.log(x);
        return m;
    }
    private static double square(double x) {
        double m = x*x;
        return m;
    }
    private static double pow(double x, double y) {
        double m = Math.pow(x,y);
        return m;
    }
    private static double divide(double x, double y) {
        double m = x/y;
        return m;
    }
    private static double multi(double x, double y) {
        double m = x*y;
        return m;
    }
    public static double plus (double x, double y) {
        double m = x+y;
        return m;}
    public static double substr(double x, double y) {
        double m = x-y;
        return m;
    }
    public static double percent(double x, double y) {
        double m = x * 0.01*y;
        return m;
    }
}
