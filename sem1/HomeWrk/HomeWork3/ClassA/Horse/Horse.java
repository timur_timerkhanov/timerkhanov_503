import java.util.Scanner;

public class Horse {
    // method that finds and returns number of possible turns from the position
    public static int Moving(int x, int y, int a[][]) {
        int mov = 0;
        if (x > 0 && y > 1 && a[x - 1][y - 2] == 0)
            mov++;
        if (x > 0 && y < a.length-2 && a[x - 1][y + 2] == 0)
            mov++;
        if (x > 1 && y > 0 && a[x - 2][y - 1] == 0)
            mov++;
        if (x > 1 && y < a.length-1 && a[x - 2][y + 1] == 0)
            mov++;
        if (x < a.length-1 && y > 1 && a[x + 1][y - 2] == 0)
            mov++;
        if (x < a.length-1 && y < a.length-2 && a[x + 1][y + 2] == 0)
            mov++;
        if (x < a.length-2 && y > 0 && a[x + 2][y - 1] == 0)
            mov++;
        if (x < a.length-2 && y < a.length-1 && a[x + 2][y + 1] == 0)
            mov++;
        return mov;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please, enter the size of desk");
        int n = sc.nextInt();
        System.out.print("Please, enter start position: ");
        System.out.print("x= ");
        int x = sc.nextInt();
        System.out.print("y= ");
        int y = sc.nextInt();
        int[][] a = new int[n][n];
        int moves=0;    //min number of moves to position
        int movesi;     //number of moves to next position that we comparing with 'moves'
        int i = 0;
        int j = 0;
        int movesNumb=0;

         //filling array with 0 in order to check if we were on the position
        for (int f = 0; f < a.length; f++) {
            for (int u = 0; u < a[f].length; u++) {
                a[u][f] = 0;
            }
        }

        //Finding path using Warnsdorf's rule:
        //on each x,y we check if it is safe to move from
        //if it's safe we call 'Moving' method
        //and write returned value to 'movesi'
        //comparing previous 'moves' with new 'movesi'
        //if it is less, changing them and remember new coordinates(i,j)
        //after all of 8 checks we choosing turn with minimal number of next possible turns
        //and rewrite x,y with that position coordinates(i,j)
        while (movesNumb < n*n) {
            movesNumb++;
            a[x][y] = movesNumb;
            moves = 9;  //knight can move only to 8 position, so take it 9
            if (x > 0 && y > 1 && a[x - 1][y - 2] == 0) {
                movesi = Moving(x - 1, y - 2, a);
                if (movesi < moves) {
                    i = x - 1;
                    j = y - 2;
                    moves = movesi;
                }
            }
            if (x > 0 && y < a.length-2 && a[x - 1][y + 2] == 0) {
                movesi = Moving(x - 1, y + 2, a);
                if (movesi < moves) {
                    i = x - 1;
                    j = y + 2;
                    moves = movesi;
                }
            }
            if (x > 1 && y > 0 && a[x - 2][y - 1] == 0) {
                movesi = Moving(x - 2, y - 1, a);
                if (movesi < moves) {
                    i = x - 2;
                    j = y - 1;
                    moves = movesi;
                }
            }
            if (x > 1 && y < a.length-1 && a[x - 2][y + 1] == 0) {
                movesi = Moving(x - 2, y + 1, a);
                if (movesi < moves) {
                    i = x - 2;
                    j = y + 1;
                    moves = movesi;
                }
            }
            if (x < a.length-1 && y > 1 && a[x + 1][y - 2] == 0) {
                movesi = Moving(x + 1, y - 2, a);
                if (movesi < moves) {
                    i = x + 1;
                    j = y - 2;
                    moves = movesi;
                }
            }
            if (x < a.length-1 && y < a.length-2 && a[x + 1][y + 2] == 0) {
                movesi = Moving(x + 1, y + 2, a);
                if (movesi < moves) {
                    i = x + 1;
                    j = y + 2;
                    moves = movesi;
                }
            }
            if (x < a.length-2 && y > 0 && a[x + 2][y - 1] == 0) {
                movesi = Moving(x + 2, y - 1, a);
                if (movesi < moves) {
                    i = x + 2;
                    j = y - 1;
                    moves = movesi;
                }
            }
            if (x < a.length-2 && y < a.length-1 && a[x + 2][y + 1] == 0) {
                movesi = Moving(x + 2, y + 1, a);
                if (movesi < moves) {
                    i = x + 2;
                    j = y + 1;
                    moves = movesi;
                }
            }
            x = i;
            y = j;
        }

        for (int f = 0; f < a.length; f++) {
            for (int u = 0; u < a[f].length; u++) {
                System.out.print(a[f][u] + "\t");
            }
            System.out.println();
        }
    }
}
