import java.util.Scanner;

public class Gauss {
    public static void main(String [] args){
        System.out.println("Please, enter the number of unknowns");
        Scanner numb = new Scanner(System.in);
        int n = numb.nextInt();
        double[][]a=new double[n][n+1];
        double[] x = new double[n];
        System.out.println("Please enter the coefficients to matrix(using space and enter) ");
        Scanner matr = new Scanner(System.in);
        //inputting the elements of array
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j<a[i].length; j++) {
                a[i][j] = matr.nextDouble();
            }
        }
        // changing lines in order to put the lines with bigger elements to top


        // making the elements below non-zero element equal to zero
        // using algorithm in accordance with the Gaussian method
        for (int i=0; i<a.length-1;i++) {
            for (int k = i+1; k < a.length; k++) {
                double m = a[k][i] / a[i][i];
                for (int j = 0; j <= a.length; j++) {
                    a[k][j] = a[k][j] - m * a[i][j];
                }
            }
        }
        // finding unknowns
        for (int i = a.length-1; i>=0; i--) {
            x[i] = a[i][n] / a[i][i];  //starting with last equation, finding x[n]
            for (int j = i+1; j < a.length; j++) {
                if (i!=n-1) {          //checking if it is not last, if last then just save
                    x[i]-= a[i][j] *x[j] / a[i][i];   //solving equation with x[i] we already know
                }
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j<a[i].length; j++) {
                System.out.print(a[i][j]+ "\t");
            }
            System.out.println();
        }

        for (int i = 0; i < x.length; i++) {
            System.out.print(x[i] + " ");
        }
    }
}
