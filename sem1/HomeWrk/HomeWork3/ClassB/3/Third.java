import java.util.Scanner;

public class Third {
    public static void main(String[] args) {
        System.out.println("Please, enter 'i'");
        Scanner i1 = new Scanner(System.in);
        int m = i1.nextInt();
        System.out.println("Please, enter 'j'");
        int n = i1.nextInt();


        int[][] x = new int[4][4];
        int f = 1;
        // initializing new variables
        // t - top left element, b -bottom left; l - left bottom, r - right bottom
        // d - its our direction
        int t = 0;
        int b = x.length - 1;
        int l = 0;
        int r = x[1].length - 1;
        int dir=0;
       /* choosing direction
        dir=0 means we're moving from top left to top right    
        dir=1 - from top right to bottom right                  
        dir=2 - from bottom right to bottom left                
        dir=3 - bottom left - top right                         



        */
        if (m==0 && n==0) {
            dir=0;
        }else
            if (m==0 && n==x.length-1) {
                dir=1;

        }else
            if (m == x[m].length - 1 && n == x.length-1) {
                dir = 2;
            }
        else
            if (m==x[m].length-1 && n==0) {
                dir=3;
            }



        // filling all elements in our direction
        // then change top/bottom elements in order to move to next spiral
        while ((t <= b) && (l <= r)) {
            switch (dir) {
                case 0:
                for (int i = l; i <= r; i++) {
                    x[t][i] = f;
                    f++; }
                    t++;
                    break;

                case 1:
                for (int i = t; i <= b; i++) {
                    x[i][r] = f;
                    f++;
                }
                    r--;
                    break;

                case 2:
                for (int i = r; i >= l; i--) {
                    x[b][i] = f;
                    f++;
                }
                    b--;
                    break;

                case 3:
                for (int i = b; i >= t; i--) {
                    x[i][l] = f;
                    f++;
                }
                l++;
                    break;

            }
            dir=(dir+1)%4;
        }
        for (int j = 0; j < x.length; j++) {
            for (int i = 0; i < x[j].length; i++) {
                System.out.print(x[j][i] + "\t");

            }
            System.out.println();


        }
    }
}
