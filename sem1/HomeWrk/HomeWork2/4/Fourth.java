public class Fourth {
    public static void main(String[] args) {
        int [] x = {1,2,3,4,5,6,7,8};
        int [] y = new int[x.length];
        int k = 6;

        //adding last k elements to the beginning
        for (int i=0; i<k; i++) {
            y[i] = x[x.length-k+i];
        }
        // filling array from k-position
        for (int i = k; i<y.length;i++) {
            y[i] = x[i - k];
        }

        for (int i=0; i<x.length; i++) {
            System.out.print(y[i]+ " ");
        }

        System.out.println();

        // doing the same to left
        for (int i=0; i<x.length-k; i++ ) {
            y[i] = x[i + k];
        }
        int f=0;
        while (f<k) {
            for (int i = x.length - k; i < x.length; i++) {

                y[i] = x[f];
                f++;

            }
        }
        for (int i=0; i<x.length; i++) {
            System.out.print(y[i] + " ");
        }

    }
}
