public class Third {
    public static void main (String[] args) {
        int [] x = {1,2,3,4,5,6,7,8,9,10};
        int [] y = new int[x.length];
        int k = 3;

        //zeroing the first k elements
        for (int i=0; i<k; i++) {
            y[i] = 0;
        }
        // filling array from k-position
        for (int i = k; i<y.length;i++) {
            y[i] = x[i - k];
        }

        for (int i=0; i<x.length; i++) {
            System.out.print(y[i]+ " ");
        }

        System.out.println();

        // doing the same to left
        for (int i=0; i<x.length-k; i++ ) {
            y[i] = x[i + k];
        }
        for (int i = x.length -k; i<x.length; i++) {
            y[i] = 0;
        }
        for (int i=0; i<x.length; i++) {
            System.out.print(y[i] + " ");
        }

    }

}
