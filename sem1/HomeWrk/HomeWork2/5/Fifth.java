public class Fifth {
    public static void main (String[] args) {
        int[] a = new int[]{1, 2, 3, 4,5,6,7,8,9,10};
        int k = 3;
        int[] b = new int[a.length];
        System.out.println("k=" +k);

        /*checking if i + k > array length
        in order to find and fill first k elements
        filling first k elements from k to 0
        */
        for (int i = a.length-1; i >= 0; i--) {
            if(i+k >= a.length){
                b[i+k-a.length] = a[i];
            }
        /* when first elements are ready
         going to the rest and filling it from the end
         */
            else{
                b[i+k] = a[i];
            }
        }
        for (int i=0;i<a.length;i++) {
            System.out.print(b[i]+" ");
        }
        System.out.println();

        // doing the same in other side
        //just replacing conditions

        for (int i =0; i < a.length; i++) {
            if(i+k < a.length){
                b[i] = a[i+k];

            }else{
                b[i] = a[i+k-a.length];
            }
        }
        for (int i=0;i<a.length;i++) {
            System.out.print(b[i] + " ");
        }
    }
}
