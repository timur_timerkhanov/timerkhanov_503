import java.util.Random;
import java.util.Scanner;

public class First {
    public static void main(String[] args){
        Scanner number = new Scanner(System.in);
        System.out.println("Please, enter size of the array ");
        int n = number.nextInt();
        int[] a = new int[n];
        /* A user choosing what he wants to see
        Putting his choice to 'p'
         */
        Scanner purpose = new Scanner(System.in);
        System.out.println("If you want to see ascending array input '1'");
        System.out.println("If you want to see descending array input '2'");
        System.out.println("Or maybe random? Then input '3' ");
		int p = purpose.nextInt();

        // checking if p = 1 or 2 or 3
		System.out.println();

        if (p==1) {
            // initializing ascending array and print it, if need be
            for (int i=0; i<n; i++) {
            a[i] = i;
                System.out.println(a[i]);
            }
        }

        if (p==2) {
            // initializing descending array and print, if need be
            for (int i=0; i<n; i++) {
                a[i] = n - i;
                System.out.println(a[i]);
            }
        }

        if (p==3) {
            // initializing random array and print it
            for (int i=0; i<n; i++) {
                Random random = new Random();
                a[i] = random.nextInt(100);
                System.out.println(a[i]);
            }

        }
    }
}