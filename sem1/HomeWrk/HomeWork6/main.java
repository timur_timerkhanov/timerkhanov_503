import java.util.Scanner;
public class main {
    public static void main(String[] args){
        System.out.println("List of operations: +, -, *, /, sqrt, sin, cos, log, ^, mod(a), c(to clear the history), h(to see history)");
        extCalc res=new extCalc();     //creating new extClass object
        Scanner sc = new Scanner(System.in);
        while (true){                     //in endless cycle
            res.opMessage();              //outputting message for choosing operation
            String str = sc.nextLine();   //inputting it
            res.check(str);               //and directing it to extCalc's check()
        }
    }
}
