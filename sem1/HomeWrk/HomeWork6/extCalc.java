import java.util.Scanner;
public class extCalc extends Calc {
    Scanner sc = new Scanner(System.in);
    public extCalc() {
        System.out.println("Please, input a number");  //the first number's inputting
        result=sc.nextDouble();                        //initializing result and history with the first number
        history=history+result;
    }
    public void check(String st) {                    //depending on chose operation go to appropriate methods
        switch (st) {
            case "+":
                inpMessage();
                numb = numbInput();
                add(numb);
                break;
            case "-":
                inpMessage();
                numb = numbInput();
                substr(numb);
                break;
            case "*":
                inpMessage();
                numb = numbInput();
                multi(numb);
                break;
            case "/":
                inpMessage();
                numb = numbInput();
                divide(numb);
                break;
            case "sqrt":
                sqrt();
                break;
            case "mod":
                inpMessage();
                numb = numbInput();
                mod(numb);
                break;
            case "^":
                inpMessage();
                numb = numbInput();
                pow(numb);
                break;
            case "%":
                inpMessage();
                numb = numbInput();
                percent(numb);
                break;
            case "cos":
                cos();
                break;
            case "sin":
                sin();
                break;
            case "log":
                log();
                break;
            case "h":
                if(history!="")
                System.out.println(history+" = " + result);
                break;
            case "c":
                System.out.println("History cleared");
                inpMessage();
                result=numbInput();
                history=Double.toString(result);

        }
    }
    public void inpMessage() {
        System.out.println("Please, input a number");
    }
    public void opMessage() {
        System.out.println("Please, choose an operation");
    }

    public void sqrt () {
        result=Math.sqrt(result);
        System.out.println(result);
        history("Sqrt");
    }
    public void mod (double newNumb) {
        result = result % newNumb;
        System.out.println(result);
        history("mod", newNumb);
    }
    public void pow (double newNumb){
        result=Math.pow(result, newNumb);
        System.out.println(result);
        history("^", newNumb);
    }
    public void percent (double newNumb) {
        result=result*0.01*newNumb;
        System.out.println(result);
        history("%",newNumb);
    }
    public void sin() {
        result=Math.sin(result);
        System.out.println(result);
        history("sin");
    }
    public void cos() {
        result=Math.cos(result);
        System.out.println(result);
        history("cos");
    }
    public void log() {
        result=Math.log(result);
        System.out.println(result);
        history("log");
    }
}
